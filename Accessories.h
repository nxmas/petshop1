#pragma once
#include"Product.h"

class Accessories:public Product
{
private:
	string description;
public:
	Accessories(string product_name, int stock_quantity, float price,string description)
		:Product(product_name, stock_quantity,price)
	{
		this->description = description;
	}
	string getDescription()
	{
		return this->description;
	}
	virtual void print()
	{
		Product::print();
		cout << "Description: " << description<< endl<<endl;
	}
};